var express = require("express");
var cors = require('cors')

var app = express();

var corsOptions = {
  'Access-Control-Allow-Origin': 'http://127.0.0.1'
}

const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://192.168.1.50:27017/";
app.get("/works", cors(corsOptions), function (request, response) {
  MongoClient.connect(url, function (err, db) {
    var dbo = db.db('portfolio');
    dbo.collection('works').find({}).sort('order', 1).toArray(function (err, result) {
      response.send(result);
    });
  });
});
app.listen(3111);
